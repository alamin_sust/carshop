<%@page import="org.apache.tomcat.util.http.fileupload.RequestContext"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.io.InputStream"%>
<%@page import="org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="com.carshop.utils.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>


        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            if (session.getAttribute("username") == null || !session.getAttribute("username").toString().equals("admin")) {
                response.sendRedirect("home.jsp");
            }

            session.setAttribute("successMsg", null);
            session.setAttribute("errorMsg", null);

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            String successMsg = "";
            String errorMsg = "";
            
            Constants constants = new Constants();
            
            String name = "";
            String price = "";
            String year = "";
            String make = "";
            String model = "";
            String ownerId = ""; 
            
            if(request.getParameter("id")!=null&&!request.getParameter("id").equals("")) {
                String q = "select * from product where id="+request.getParameter("id");
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            rs.next();
                
               name = rs.getString("name");
            price = rs.getString("price");
            year = rs.getString("year");
            make = rs.getString("make");
            model = rs.getString("model");
            ownerId = rs.getString("ownerId"); 
            }
            


            if(ServletFileUpload.isMultipartContent(request)) {
            
            String q = "select max(id)+1 as mxId from product";
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            rs.next();
            String mxId=rs.getString("mxId");
            
            

            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest((RequestContext) request);

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString();
                    // ... (do your job here)
                    if(fieldname.equals("ownerId")){
                        ownerId=fieldvalue;
                    }
                    else if(fieldname.equals("name")){
                        name=fieldvalue;
                    }
                    else if(fieldname.equals("price")){
                        price=fieldvalue;
                    }
                    else if(fieldname.equals("year")){
                        year=fieldvalue;
                    }
                    else if(fieldname.equals("make")){
                        make=fieldvalue;
                    }
                    else if(fieldname.equals("model")){
                        model=fieldvalue;
                    }
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    //String filename = FilenameUtils.getName(item.getName());
                    if (fieldname.equals("file")) {
                        InputStream filecontent = item.getInputStream();
                        // ... (do your job here)
//                        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
//                        Calendar cal = Calendar.getInstance();
                        //System.out.println(dateFormat.format(cal.getTime()));

                        File bfile = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\CarShop\\web\\img\\products\\" + ownerId+"_" +mxId+ ".jpg");
                        File bfileBuild = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\CarShop\\build\\web\\img\\products\\" + ownerId+"_" +mxId+ ".jpg"); 
                        //out.print(getServletContext().getRealPath("/"));
                        OutputStream outStream = new FileOutputStream(bfile);
                        OutputStream outStreamBuild = new FileOutputStream(bfileBuild);

                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = filecontent.read(buffer)) > 0) {
                            outStream.write(buffer, 0, length);
                            outStreamBuild.write(buffer, 0, length);
                        }
                        filecontent.close();
                        outStream.close();
                        outStreamBuild.close();


                        //String query2 = "insert into pillar_updated_by(soldier_id,pillar_id,img_url,longitude,latitude,battalion,imie_number) values(" + soldierId + ","+id+", '" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg" + "','"+longitude+"','"+latitude+"','"+ ConverterUtil.getBattalionFromImie(authImie)+"','"+authImie+"')";

                        //Statement st2 = db.connection.createStatement();

                        //st2.executeUpdate(query2);
                    }
                }
            }


            //String query = "insert into pillar(id,name,number,longitude,latitude,situation) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"', '"+situation+"')";

            String insertProduct = request.getParameter("insert")==null?"":request.getParameter("insert");
            String updateProduct = request.getParameter("update")==null?"":request.getParameter("update");
            
            String query;
            Statement stMig = db.connection.createStatement();

            if(!insertProduct.equals("")) {
                String values="";
                List<String> tables = constants.getDB_TABLES();
                for(int i=0;i<tables.size();i++) {
                    if(i>0) {
                        values+=",";
                    }
                    values+="'"+request.getParameter(tables.get(i))+"'";
                }
                
                q2 = "insert into product (id,name,price,year,make,model,ownerId,"
                        +constants.getCommaSeparatedTableNames()+") values("
                        +mxId+",'"
                        +name+"',"+price
                        +",'"+year+"','"+make
                        +"','"+model+"',"+ownerId
                        +","+values+")";
                
                st2 = db.connection.createStatement();
                st2.executeUpdate(q2);
                
                //session.setAttribute("successMsg", "Product Added Successfully!");
                
            } else if(!updateProduct.equals("")) {
                query = "update product  set "+
                        " name='"+name
                        +"', price="+price
                        +", year='"+year
                        +"', make='"+make
                        +"', model='"+model
                        +"', ownewId="+ownerId+" where id="+request.getParameter("productId");
                stMig.executeUpdate(query);
                //session.setAttribute("successMsg","Successfully Updated!");
            }
                
                
            }


        %>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
<!--                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>-->
                </div>
                <div class="col-sm-8 well text-left" >
                    <%if (session.getAttribute("successMsg") != null) {%>
                    <div class="alert alert-success">
                        <Strong><%=session.getAttribute("successMsg")%></Strong>
                    </div>
                    <%}%>
                    <%if (session.getAttribute("errorMsg") != null) {%>
                    <div class="alert alert-danger">
                        <Strong><%=session.getAttribute("errorMsg")%></Strong>
                    </div>
                    <%}
                        session.setAttribute("successMsg", null);
                        session.setAttribute("errorMsg", null);

                    %>
                    
                    <div class="row" >





                        <form action="addProduct.jsp" method="post"  enctype="multipart/form-data">
                            
                            <div class="form-group input-group col col-sm-8 col-sm-offset-2">
                                <div class="row">
                                    <label>Vehicle Image</label><input name="file" type="file" class="form-control" required=""/>
                                </div>
                                <%if(request.getParameter("id")!=null&&!request.getParameter("id").equals("")) {%>
                                <img src="img/products/<%=ownerId%>_<%=request.getParameter("id")%>.jpg" width="150px;" height="100px;" class="img-responsive img-circle center-block" />
                                <%}%>
                                <div class="row">
                                    <label>Name</label><input name="name" value="<%=name%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Price</label><input name="price" value="<%=price%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Year</label><input name="year" value="<%=year%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Make</label><input name="make" value="<%=make%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Model</label><input name="model" value="<%=model%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>


                                <%for (int i = 0; i < constants.getDB_TABLES().size(); i++) {%>
                                <div class="row">
                                    <label><%=constants.getSEARCH_CRITERIA().get(i)%></label>
                                    <select class="form-control" name="<%=constants.getDB_TABLES().get(i)%>" required="">
                                        <option value="">--Select--</option>
                                        <%
                                            Statement stType = db.connection.createStatement();
                                            String qType = "select * from " + constants.getDB_TABLES().get(i) + " where id>0";
                                            ResultSet rsType = stType.executeQuery(qType);
                                            while (rsType.next()) {
                                        %>
                                        <option value="<%=rsType.getString("id")%>"><%=rsType.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <br>
                                <%}%>
                                <div class="row">
                                    <label>Owner</label>
                                    <select class="form-control" name="ownerId" required="">
                                        <option value="">--Select--</option>
                                        <%
                                            Statement stO = db.connection.createStatement();
                                            String qO = "select * from user where id>0 and type='seller'";
                                            ResultSet rsO = stO.executeQuery(qO);
                                            while (rsO.next()) {
                                        %>
                                        <option value="<%=rsO.getString("id")%>" <%if(request.getParameter("id")!=null && request.getParameter("id").equals(rsO.getString("id"))){%>selected<%}%>><%=rsO.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <br>

                                <div class="row">
                                    <span class="input-group-btn">
                                        <%if(request.getParameter("id")!=null && !request.getParameter("id").equals("")){%>
                                        <input type="hidden" name="productId" value="<%=request.getParameter("id")%>"/>
                                        <input type="hidden" name="update" value="true"/>
                                        <button type="submit" class="btn btn-primary col col-sm-12" type="button">
                                            <span class="glyphicon glyphicon-edit">  Update</span>
                                        </button>
                                        <%} else {%>
                                        <input type="hidden" name="insert" value="true"/>
                                        <button type="submit" class="btn btn-primary col col-sm-12" type="button">
                                            <span class="glyphicon glyphicon-plus">  Insert</span>
                                        </button>
                                        <%}%>
                                        
                                    </span>
                                </div>        
                            </div>
                        </form>


                    </div>



                </div>
                <div class="col-sm-2 sidenav">
<!--                    <div class="well">
                        <p>ADS</p>
                    </div>
                    <div class="well">
                        <p>ADS</p>
                    </div>-->
                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->
