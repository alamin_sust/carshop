<%@page import="com.carshop.connection.Database"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Auto Car an Auto Mobile Category Bootstrap Responsive Website Template | Home :: w3layouts</title>

        <!-- Meta tag Keywords -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Auto Car Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--// Meta tag Keywords -->

        <!-- css files -->
        <link href="css/mislider.css" rel="stylesheet" type="text/css" />
        <link href="css/mislider-custom.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> <!-- Bootstrap-Core-CSS -->
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> <!-- Style-CSS --> 
        <link rel="stylesheet" href="css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
        <!-- //css files -->

        <!-- online-fonts -->
        <link href="//fonts.googleapis.com/css?family=Jockey+One&amp;subset=latin-ext" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Niconne&amp;subset=latin-ext" rel="stylesheet">
        <!-- //online-fonts -->
    </head>

    <body>
        <%

            Database db = new Database();
            db.connect();

            // try {
        %>
        <!-- banner -->
        <div class="banner wthree">
            <div class="container">
                <div class="banner_top">
                    <div class="logo wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                        <h1><a class="navbar-brand" style="width:200px;" href="home.jsp"><img src="img/logo-white-t.png" style="width: 150px; height: 35px;"/></a></h1>
                    </div>
                    <div class="banner_top_right wow fadeInRight animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav cl-effect-14">
                                    <li><a href="index.html" class="active">Home</a></li>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="gallery.html">Gallery</a></li>
                                    <li><a href="codes.html">Codes</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                        <%if (session.getAttribute("username") == null) {%>
                                    <li><a href="loginRegister.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                                        <%} else {%>
                                    <li><a href="profile.jsp">logged in as: <strong><%=session.getAttribute("username")%></strong></a></li>
                                    <li><a href="loginRegister.jsp"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                                    <%}%>
                                </ul>
                            </div><!-- /.navbar-collapse -->	

                        </nav>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <!-- banner -->