/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carshop.utils;

import java.util.List;

/**
 *
 * @author md_al
 */
public class Statics {
    //success
    public static final String LOGIN_SUCCEESS = "Successfully Logged In!";
    public static final String REGISTER_SUCCEESS = "Registration Successfull!";
    
    //error
    public static final String LOGIN_USERNAME_NOT_EXISTS = "Username doesn't exists!";
    public static final String LOGIN_MISSMATCH = "Username and Password doesn't matches!";
    public static final String REGISTER_USERNAME_ALREADY_EXISTS = "Username already exists!";
    public static final String REGISTER_PASSWORD_MISSMATCH = "Passwords doesn't matches!";
    
    
    
    
}
